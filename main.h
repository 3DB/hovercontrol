#pragma once

/****************************************************
 *                                                  *
 *                     DEFINES                      *
 *                                                  *
\****************************************************/

// Stuff
#define MAIN_LOOP_DELAY 2

// Pins
#define PIN_FORWARD     PB1        // Poti forwards
#define PIN_BRAKE       PB0        // Poti brake
#define PIN_UP          PB15       // Poti mode up
#define PIN_DOWN        PA8        // Poti mode down
#define PIN_LED         PC13       // Board LED
#define PIN_UN_R        PA4        // Underground neon red
#define PIN_UN_G        PA5        // Underground neon green
#define PIN_UN_B        PA6        // Underground neon blue

// UART
#define CONTROL_BAUD_RATE 19200

// I2C
#define I2C_ADDRESS_HOVERBOARD 42   // I2C address of the hoverboard mainboard

// Driving
#define DEFAULT_MODE 3              // Default mode in which the vehicle begins
#define TURBO_TIME_TO_PRESS 3000    // Time to press mode up to switch into TURBO

// Inputs
#define FORWARD_MIN_VAL  1070   // Min value to process
#define FORWARD_MAX_VAL  3130   // Max value to process
#define BRAKE_MIN_VAL    1070   // Min value to process
#define BRAKE_MAX_VAL    3170   // Max value to process

// Funcs
#define CLAMP(x, low, high) (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))


/****************************************************
 *                                                  *
 *                   NOT SETTINGS                   *
 *                                                  *
\****************************************************/

// ADC Deltas
#define FORWARD_DELTA (FORWARD_MAX_VAL - FORWARD_MIN_VAL)
#define BRAKE_DELTA (BRAKE_MAX_VAL - BRAKE_MIN_VAL)

// Accelerations
#define NO_INPUT_BRAKE_ACC 0.996f  // nearer to 1 -> slower
#define PRESS_ACC (1.0f - NO_INPUT_BRAKE_ACC + 0.001f)  // nearer to 0 -> slower


/****************************************************
 *                                                  *
 *                     FUNCTIONS                    *
 *                                                  *
\****************************************************/

void setup(void);
void loop(void);
void on_mode_up_pressed(void);
void on_mode_up_released(void);
void on_mode_down_pressed();
void on_mode_down_released(void);
void send_UART_packet(void);
void calc_checksum(void);
void send_I2C_packet(void);
