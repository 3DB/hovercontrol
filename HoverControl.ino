#include <I2C.h>
#include <FastLED.h>
// #define DEBUG
// #define LEDANALOG
#define LEDDIGITAL

// Pins
#define PIN_FORWARD     A0         // Poti forwards
#define PIN_BRAKE       A1         // Poti brake
#define PIN_UN_R        3          // Underground neon red
#define PIN_UN_G        5          // Underground neon green
#define PIN_UN_B        6          // Underground neon blue

// Feature Pins
#define PIN_F_STVO      7          // Use mode 1
#define PIN_F_CHILD     8          // Use mode -1
#define PIN_F_LED       9          // Disables LEDs

// Inputs
#define FORWARD_MIN_VAL  200   // Min value to process
#define FORWARD_MAX_VAL  860   // Max value to process
#define BRAKE_MIN_VAL    200   // Min value to process
#define BRAKE_MAX_VAL    860   // Max value to process

// Movement
#define I2C_ADDRESS_HOVERBOARD 42 //  21   // I2C address of the hoverboard mainboard
#define DEFAULT_MODE    3              // Default mode in which the vehicle begins

// Funcs
#define CLAMP(x, low, high) (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))

// ADC Deltas
#define FORWARD_DELTA (FORWARD_MAX_VAL - FORWARD_MIN_VAL)
#define BRAKE_DELTA (BRAKE_MAX_VAL - BRAKE_MIN_VAL)

// Accelerations
#define NO_INPUT_BRAKE_ACC 0.975f  // nearer to 1 -> slower
#define PRESS_ACC (1.0f - NO_INPUT_BRAKE_ACC + 0.012f + 0.001f)  // nearer to 0 -> slower
#define PRESS_ACC2 (1.0f - NO_INPUT_BRAKE_ACC + 0.008f + 0.001f)

// LED foo
#define NUM_LEDS 58
#define R_UNTERBODEN_BEGIN 0
#define R_BLINKER_LED_BEGIN 25
#define L_BLINKER_LED_BEGIN 29
#define L_UNTERBODEN_BEGIN 33
#define DATA_PIN 6

typedef struct {
  int16_t speedL;
  int16_t speedR;
  int16_t weakL;
  int16_t weakR;
  int16_t checksum;
} I2cmd;

I2cmd command;

int mode = DEFAULT_MODE;
int16_t speed = 0;      // Current speed (-1000 to 1000)
int16_t weak = 0;
long lastBackBlink = 0;
volatile int speedForLeds = 0;
bool blinkOn = true;

// Feature defaults
bool bStVO =        false;
bool bChild =       false;
bool bLEDs =        true;

#ifdef LEDDIGITAL
  CRGB leds[NUM_LEDS]; // LEDS
  int timeRunning = 0;
  int timeRunningBackwards = 0;
  int timeWhiteLed = 1;
  bool green = true;
  int whiteLedIndex = 24;
  int curBlue = 0;
#endif

// DEBUG
int dSpeed = 0;
bool dAsc = true;

void setup()
{
  I2c.begin();
  Serial.begin(19200);
  Serial.write("starting");
  // Wire.begin();
  I2c.timeOut(10);

  pinMode(PIN_FORWARD, INPUT);
  pinMode(PIN_BRAKE, INPUT);
  pinMode(PIN_F_STVO, INPUT_PULLUP);
  pinMode(PIN_F_CHILD, INPUT_PULLUP);
  pinMode(PIN_F_LED, INPUT_PULLUP);

  // Features
  if (!digitalRead(PIN_F_STVO)) {
    bStVO = !bStVO;
  } if (!digitalRead(PIN_F_CHILD)) {
    bChild = !bChild;
  } if (!digitalRead(PIN_F_LED)) {
    bLEDs = !bLEDs;
  }

  if (bStVO) {
    mode = 1;
  } else if (bChild) {
    mode = -1;
  }
  
  #ifdef LEDANALOG
    pinMode(PIN_UN_R, OUTPUT);
    pinMode(PIN_UN_G, OUTPUT);
    pinMode(PIN_UN_B, OUTPUT);
  #endif

  #ifdef LEDDIGITAL
    if (bLEDs) {
      cli();//stop interrupts
      
      TCCR1A = 0;// set entire TCCR1A register to 0
      TCCR1B = 0;// same for TCCR1B
      TCNT1  = 0;//initialize counter value to 0
      OCR1A = 1000;// = (16*10^6) / (1*1024) - 1 (must be <65536)    
      TCCR1B |= (1 << WGM12);
      // Set CS10 and CS12 bits for 1024 prescaler
      TCCR1B |= (1 << CS11) | (1 << CS10);  
      // enable timer compare interrupt
      TIMSK1 |= (1 << OCIE1A);
      sei(); // enable Interrupts
    }
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);

    for (int i = 0; i < NUM_LEDS; i++) {
      leds[i].setRGB(0, 0, 0);
    }
  #endif
}


void loop()
{
    delay(8);
    #ifdef DEBUG
      Serial.println("calc");
    #endif
    Serial.println("calc2");

    // Calculate speed
    int val_forward = analogRead(PIN_FORWARD);
    int val_break = analogRead(PIN_BRAKE);
    #ifdef DEBUG
      Serial.print("val_forward ");
      Serial.println(val_forward);
      Serial.print("val_break ");
      Serial.println(val_break);
    #endif

    switch (mode) {
        case -1:     // MAKER FAIRE CHILD MODE (5 km/h?)
          speed = (float)speed * NO_INPUT_BRAKE_ACC
                - (CLAMP(val_break - BRAKE_MIN_VAL, 0, BRAKE_DELTA) / (BRAKE_DELTA / 150.0f)) * PRESS_ACC2
                + (CLAMP(val_forward - FORWARD_MIN_VAL, 0, FORWARD_DELTA) / (FORWARD_DELTA / 250.0f)) * PRESS_ACC;
        break;
      
      case 0:     // BACKWARDS
        speed = (float) speed * NO_INPUT_BRAKE_ACC
                + (CLAMP(val_break - BRAKE_MIN_VAL, 0, BRAKE_DELTA) / (BRAKE_DELTA / 250.0f)) * PRESS_ACC2
                - (CLAMP(val_forward - FORWARD_MIN_VAL, 0, FORWARD_DELTA) / (FORWARD_DELTA / 350.0f)) * PRESS_ACC;
        break;
  
      case 1:     // StVO
        speed = (float)speed * NO_INPUT_BRAKE_ACC
                - (CLAMP(val_break - BRAKE_MIN_VAL, 0, BRAKE_DELTA) / (BRAKE_DELTA / 200.0f)) * PRESS_ACC2
                + (CLAMP(val_forward - FORWARD_MIN_VAL, 0, FORWARD_DELTA) / (FORWARD_DELTA / 350.0f)) * PRESS_ACC;
        break;
  
      case 2:     // NORMAL SPEED
        speed = (float)speed * NO_INPUT_BRAKE_ACC
                - (CLAMP(val_break - BRAKE_MIN_VAL, 0, BRAKE_DELTA) / (BRAKE_DELTA / 340.0f)) * PRESS_ACC2
                + (CLAMP(val_forward - FORWARD_MIN_VAL, 0, FORWARD_DELTA) / (FORWARD_DELTA / 600.0f)) * PRESS_ACC;
        break;
  
      case 3:     // TOP SPEED
        speed = (float)speed * NO_INPUT_BRAKE_ACC
                - (CLAMP(val_break - BRAKE_MIN_VAL, 0, BRAKE_DELTA) / (BRAKE_DELTA / 400.0f)) * PRESS_ACC2
                + (CLAMP(val_forward - FORWARD_MIN_VAL, 0, FORWARD_DELTA) / (FORWARD_DELTA / 1000.0f)) * PRESS_ACC;
        break;
  
      case 4:     // TURBO
        speed = (float)speed * NO_INPUT_BRAKE_ACC
                - (CLAMP(val_break - BRAKE_MIN_VAL, 0, BRAKE_DELTA) / (BRAKE_DELTA / 340.0f)) * PRESS_ACC2
                + (CLAMP(val_forward - FORWARD_MIN_VAL, 0, FORWARD_DELTA) / (FORWARD_DELTA / 1000.0f)) * PRESS_ACC;
        
        break;
    }
    
    #ifdef DEBUG
      Serial.print("speed ");
      Serial.println(speed);
    #endif

    weak *= 0.95;
    if (mode == 4 && speed > 800) {
      weak += 400.0 * 0.05;
    }

    /*
    if (dAsc) {
      dSpeed++;
    } else {
      dSpeed--;
    }

    speed = dSpeed;

    if (dSpeed == 200) {
      dAsc = !dAsc;
    } else if (dSpeed == 0) {
      dAsc = !dAsc;
    }
    */

    speed = CLAMP(speed, -1000, 1000);
    weak = CLAMP(weak, 0, 400);

    // Speed for LED Interrupt
    speedForLeds = speed;

    if (bLEDs) {
      #ifdef LEDANALOG
        // Calculate underground neons
        if (speed < 0) {
          if (millis() - 500 > lastBackBlink) {
            lastBackBlink = millis();
            if (lastBackBlink % 1000 > 500) {
              analogWrite(PIN_UN_R, 255);
              analogWrite(PIN_UN_G, 255);
            } else {
              analogWrite(PIN_UN_R, 0);
              analogWrite(PIN_UN_G, 0);
            }
            analogWrite(PIN_UN_B, 0);
          }
        } else if (speed < 500) {
          analogWrite(PIN_UN_R, 0);
          analogWrite(PIN_UN_G, abs(speed) / 2 + 5);
          analogWrite(PIN_UN_B, (500 - abs(speed)) / 2 + 5);
        } else {
          analogWrite(PIN_UN_R, (abs(speed) - 500) / 2 + 5);
          analogWrite(PIN_UN_G, (1000 - abs(speed)) / 2 + 5);
          analogWrite(PIN_UN_B, 0);
        }
      #endif     
    } else {
      #ifdef LEDDIGITAL
      for (int i = 0; i < NUM_LEDS; i++) {
        leds[i].setRGB(0, 0, 0);
      }
      FastLED.show();
      #endif
    }
    
    // Build I2C packet
    command.speedL = speed;
    command.speedR = speed;
    command.weakL = weak;
    command.weakR = weak;
    command.checksum = command.speedL ^ command.speedR ^ command.weakL ^ command.weakR;
    
    #ifdef DEBUG
    Serial.print("W1");
    #endif

    #ifdef DEBUG
    Serial.print("W2");
    #endif
    
    I2c.start();
    I2c.sendAddress(42);
    for (int i = 0; i < sizeof(command); i++) {
      I2c.sendByte(((byte*)&command)[i]);
    }
    I2c.stop();
    
    #ifdef DEBUG
    Serial.print("W3");
    #endif
}

#ifdef LEDDIGITAL
  void clearLED() {
      for (int i = 0; i < NUM_LEDS; i++) {
        leds[i] = CHSV(0,0,0);
    }
  }

  // Deprecated
  void sinus(int t){
    for(int i = 0; i < NUM_LEDS; i++) {
      leds[i] = CHSV(sin((i + (t * 0.01)) * 0.01) * 255, 255, 255);
    }
  }

  ISR(TIMER1_COMPA_vect){
    if (speedForLeds > 800) {
      timeRunning++;
      if (timeRunning % 60 == 0) {
        if (timeRunning == 60) {
          for (int i = 0; i < R_BLINKER_LED_BEGIN; i++) {
            if (i < R_BLINKER_LED_BEGIN / 4) {
              leds[i].setRGB(0, 0, 255);
            } else if (i < R_BLINKER_LED_BEGIN / 2) {
              leds[i].setRGB(255, 0, 0); 
            } else if (i < R_BLINKER_LED_BEGIN - R_BLINKER_LED_BEGIN / 4) {
              leds[i].setRGB(0, 0, 255);
            } else {
              leds[i].setRGB(255, 0, 0);
            }
          }
        } else {
          for (int i = 0; i < R_BLINKER_LED_BEGIN; i++) {
            if (i < R_BLINKER_LED_BEGIN / 2) {
              leds[i].setRGB(255, 0, 0);
            } else if (i < R_BLINKER_LED_BEGIN / 2) {
              leds[i].setRGB(0, 0, 255); 
            } else if (i < R_BLINKER_LED_BEGIN - R_BLINKER_LED_BEGIN / 4) {
              leds[i].setRGB(255, 0, 0);
            } else {
              leds[i].setRGB(0, 0, 255);
            }
          }

          timeRunning = 0;
        }

        for (int i = 0; i < R_BLINKER_LED_BEGIN; i++) {
          leds[NUM_LEDS - 1 - i] = leds[R_BLINKER_LED_BEGIN - 1 - i];
        }
      }
    } else {
      timeRunning++;
      // move to left
      if(timeRunning > 10) {
        if (speedForLeds < 0) {
          if (timeRunningBackwards < 12) {
            for (int i = 0; i < 25; i++) {
              leds[i].setRGB(0, 0, 0);
            }
          } else if (timeRunningBackwards < 24) {
            for (int i = 0; i < 25; i++) {
              leds[i].setRGB(255, 255, 0);
            }
          } else {
            timeRunningBackwards = 0;
          }
          timeRunningBackwards++;
        } else {
          leds[24] = CHSV(180 - map(speedForLeds, 0, 1000, 0, 180) , 255, 255);
          memmove(&leds[0], &leds[1], 24 * sizeof(CRGB));
        }
        timeRunning = 0;
      }
  
      if (speedForLeds > 0) {
        if (whiteLedBySpeed(speedForLeds, timeRunning * timeWhiteLed)) {
          if (whiteLedIndex < 24) {
            leds[whiteLedIndex + 1] = leds[whiteLedIndex];
          }
          leds[whiteLedIndex].setRGB(255, 255, 255);
          whiteLedIndex--;
          if (whiteLedIndex < 0) {
            whiteLedIndex = 24;
          }
  
          if (timeWhiteLed == 6) {
            timeWhiteLed = 1;
          }
        }
      }
  
      for (int i = 0; i < R_BLINKER_LED_BEGIN; i++) {
        leds[NUM_LEDS - 1 - i] = leds[i];
      }
    }
    
    FastLED.show(); 
  }

  bool whiteLedBySpeed(int s, int t) {
    if (s < 200) {
      return t % 20 == 0;
    } else if (s < 400) {
      return t % 17 == 0;
    } else if (s < 600) {
      return t % 14 == 0;
    } else if (s < 800) {
      return t % 11 == 0;
    } else  {
      return t % 8 == 0;
    }
  }
  
#endif
